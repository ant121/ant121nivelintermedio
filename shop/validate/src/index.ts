import Joi from 'joi';
import * as T from './types';

export async function create(params: T.Create.Request): Promise<T.Create.Response> {

    try{

        const schema = Joi.object({
            user: Joi.number().required(),
            product: Joi.number().required(),
        });

        const result = await schema.validateAsync(params);

        return {statusCode: 'success', data: params};

    }catch (error) {

        console.error( { step: "Validation create", error: error.toString() } );

        throw { statusCode: 'error', message: error.toString() };
    }

}

export async function del(params: T.Delete.Request): Promise<T.Delete.Response> {

    try{

        const schema = Joi.object({
            ids: Joi.array().items(Joi.number().required()),
            users: Joi.array().items(Joi.number().required()),
            products: Joi.array().items(Joi.number().required())
        }).xor('ids', 'users', 'products');

        const result = await schema.validateAsync(params);

        return {statusCode: 'success', data: params};

    }catch (error) {

        console.error( { step: "Validation create", error: error.toString() } );

        throw { statusCode: 'error', message: error.toString() };
    }

}

export async function view(params: T.View.Request): Promise<T.View.Response> {

    try{

        const schema = Joi.object({
            offset: Joi.number(),
            limit: Joi.number(),
            users: Joi.array().items(Joi.number().required()),
            products: Joi.array().items(Joi.number().required())
        });

        const result = await schema.validateAsync(params);

        return {statusCode: 'success', data: params};

    }catch (error) {

        console.error( { step: "Validation view", error: error.toString() } );

        throw { statusCode: 'error', message: error.toString() };
    }

}
