const api = require('../dist');

const redis = {
    local: {
        host: "192.168.60.101",
        port: 6379,
        // password: undefined
    }
}

const environment = redis.local;

const users = [
    {username: 'ant121', fullName: 'Anthony', image: 'https://image.com', phone: 980331887},
    {username: 'elvismpq', fullName: 'Elvis', image: 'https://image.com', phone: 980331887},
    {username: 'andysp98', fullName: 'Steven', image: 'https://image.com', phone: 980331887}
]

async function create(user) {

    try {

        const result = await api.Create(user, environment);

        console.log(result);

    }catch (error) { console.error(error) }

}

async function del(username) {

    try {

        const result = await api.Delete({username}, environment);

        console.log(result);

    }catch (error) { console.error(error) }

}

async function update(params) {

    try {

        const result = await api.Update(params, environment);

        console.log(result);

    }catch (error) { console.error(error) }

}

async function findOne(params) {

    try {

        const result = await api.FindOne(params, environment);

        console.log(result);

    }catch (error) { console.error(error) }

}

async function view(params) {

    try {

        const result = await api.View(params, environment);

        console.log(result);

    }catch (error) { console.error(error) }

}

async function main() {
    try{

        await view();

        await create(users[2]);

        await view();

    }catch (e) {
        console.log(e);
    }
}

main();