export type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'ValidationError';

export const endpoint = ['create', 'delete', 'view'] as const;

export type Endpoint = typeof endpoint[number];

export interface REDIS {
        host: string;
        port: number;
        password: string;
}

export interface Model {

    id?: number;
    user?: string;
    product?: string;
    createdAt?: string;
    updatedAt?: string;

}

export interface Paginate {
    data: Model[],
    itemCount: number,
    pageCount: number,
}

export namespace Create {

    export interface Request{
        user: string;
        product: string;
    }

    export interface Response {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }

}

export namespace Delete {

    export interface Request {
        ids?: number[];
        users?: string[];
        products?: string[];
    }

    export interface Response {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }

}

export namespace View {

    export interface Request {
        offset?: number;
        limit?: number;

        users?: string[];
        products?: string[];
    }

    export interface Response {
        statusCode: statusCode;
        data?: Paginate;
        message?: string;
    }

}