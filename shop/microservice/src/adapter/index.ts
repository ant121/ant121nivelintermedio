import { name, RedisOptsQueue as opts, Actions } from "../settings";
import * as Services from '../service';
import { Worker, Job } from 'bullmq';
import {Adapters} from "../types";
import {Publish} from "../controller";


export const Process = async(job: Job<any, any, Adapters.Endpoint>) => {

    try{

        switch (job.name) {

            case 'create': {

                let {statusCode, data, message} = await  Services.create(job.data);

                return {statusCode, data, message};
            }

            case 'delete': {

                let {statusCode, data, message} = await  Services.del(job.data);

                return {statusCode, data, message};
            }

            case 'view': {

                let {statusCode, data, message} = await  Services.view(job.data);

                return {statusCode, data, message};
            }

            default: return {statusCode: 'error', message: 'Method not found'};
        }

    }catch (error) {

        await Publish({channel: Actions.error, instance: JSON.stringify({ step: 'Adapters Proccess', error })});

    }

}

export async function run () {

    try{

        await Publish({channel: Actions.start, instance: JSON.stringify({ step: 'Adapters Run', message: `starting ${name}` })});

        const worker = new Worker(`${name}:2`, Process, {connection: opts.redis, concurrency: opts.concurrency});

        worker.on('error', async error => {
            await Publish({channel: Actions.error, instance: JSON.stringify({ step: 'Adapters run', error})});
        })

    }catch (error) {

        await Publish({channel: Actions.error, instance: JSON.stringify({ step: 'Adapters run', error })});

    }

}
