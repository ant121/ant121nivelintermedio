import * as T  from '../types';
import * as Models from "../models";
import {InternalError} from "../settings";
import * as Controllers from "../controller";
import * as Validation from "validate-shop";
// import * as Validation from "../../../validate/dist";

export async function create(params: T.Service.Create.Request): Promise<T.Service.Create.Response> {

    try{

        await Validation.create(params);

        //Validacion del usuario
        const user = (await Controllers.ValidateUser({user: params.user})).data;

        if (!user.state) throw { statusCode: 'notPermitted', message: 'El usuario no esta habilitado para usar compras'}

        //Validacion del producto
        const product = (await Controllers.ValidateProduct({product: params.product})).data;

        if (!product.state) throw { statusCode: 'notPermitted', message: 'El producto no esta disponible'}

        const {statusCode, data, message} = await Models.create(params);

        return {statusCode, data, message};

    }catch (error) {

        console.error( { step: "Service create", error: error.toString() } );

        return { statusCode: 'error', message: InternalError};
    }

}

export async function del(params: T.Service.Delete.Request): Promise<T.Service.Delete.Response> {

    try{

        await Validation.del(params);

        let where: T.Models.Where = { };

        var optionals:  T.Models.Attributes[] = ['id', 'product', 'user'];

        for (let x of optionals.map( v => v.concat('s'))) if ( params[x] !== undefined ) where[x.slice(0-1)] = params[x];

        const {statusCode, data, message} = await Models.del({ where });

        if (statusCode !== 'success') return {statusCode, message};

        return { statusCode: 'success', data };

    }catch (error) {

        console.error( { step: "Service create", error: error.toString() } );

        return { statusCode: 'error', message: InternalError};
    }

}

export async function view(params: T.Service.View.Request): Promise<T.Service.View.Response> {

    try{

        await Validation.view(params);

        let where: T.Models.Where = { }

        const optionals: T.Models.Attributes[] = ['user', 'product'];

        for (let x of optionals.map( v => v.concat('s'))) if ( params[x] !== undefined ) where[x.slice(0-1)] = params[x];



        const {statusCode, data, message} = await Models.findAndCountAll(   {where} );

        if (statusCode !== 'success') return { statusCode, message };

        return { statusCode: 'success', data: data};

    }catch (error) {

        console.error( { step: "Service view", error: error.toString() } );

        return { statusCode: 'error', message: InternalError};
    }

}
