import { InternalError, redisClient, RedisOptsQueue as opts} from "../settings";
import { Controller as T} from "../types";
import * as apiUser from 'api-users';
import * as apiProduct from 'api-product';

export async function Publish(props: T.Publish.Request): Promise<T.Publish.Response> {

    try{

        if (!redisClient.isOpen) await redisClient.connect();

        await redisClient.publish(props.channel, props.instance);

        return {statusCode: 'success', data: props}

    }catch (error) {

        console.error('error', { step: 'controller Publish', error});

        return { statusCode: 'error', message: InternalError};

    }
}

export async function ValidateUser(props: T.ValidateUser.Request): Promise<T.ValidateUser.Response> {

    try{

        const {statusCode, data, message} = await apiUser.FindOne({id: props.user}, opts.redis);

        if(statusCode !== 'success') {

            switch (statusCode) {

                case 'notFound': throw { statusCode: 'validationError', message: 'No existe el usuario que validas'};

                default: throw { statusCode: 'error', message};
            }

        }

        return { statusCode: 'success', data};

    }catch (error) {

        console.error('error', { step: 'controller Publish', error});

        return { statusCode: 'error', message: InternalError};

    }
}

export async function ValidateProduct(props: T.ValidateProduct.Request): Promise<T.ValidateProduct.Response> {

    try{

        const {statusCode, data, message} = await apiProduct.FindOne({id: props.product}, opts.redis, undefined);

        if(statusCode !== 'success') {

            switch (statusCode) {

                case 'notFound': throw { statusCode: 'validationError', message: 'No existe el producto que validas'};

                default: throw { statusCode: 'error', message};
            }

        }

        return { statusCode: 'success', data};

    }catch (error) {

        console.error('error', { step: 'controller Publish', error});

        return { statusCode: 'error', message: InternalError};

    }
}