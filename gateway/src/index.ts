import express, {Express} from "express";
import http from "http";
import {Server} from "socket.io";
import {redis} from "./settings";

import * as apiUser from "api-users";
import * as apiProduct from "api-product";
import * as apiShop from "api-shop";

const app: Express = express();

const server: http.Server = http.createServer();

const io = new Server(server);

server.listen(80, () => {
        console.log("server initialize");

        io.on('connection', socket => {

            console.log("New conecction", socket.id);

            //User
            socket.on('req:user:view', async (params: apiUser.T.View.Request) => {

                try {
                    console.log('req:user:view');

                    const {statusCode, data, message} = await apiUser.View(params, redis);

                    return io.to(socket.id).emit('res:user:view', {statusCode, data, message});

                } catch (error) {
                    console.log(error)
                }

            })

            socket.on('req:user:create', async (params: apiUser.T.Create.Request) => {

                try {
                    console.log('req:user:create');

                    const {statusCode, data, message} = await apiUser.Create(params, redis);

                    return io.to(socket.id).emit('res:user:create', {statusCode, data, message});

                } catch (error) {
                    console.log(error)
                }

            })

            //Product
            socket.on('req:product:view', async (params: apiProduct.T.View.Request) => {

                try{
                    console.log('req:product:view');

                    const {statusCode, data, message} = await apiProduct.View(params, redis);

                    return io.to(socket.id).emit('res:product:view', {statusCode, data, message});

                }catch (error) { console.log(error) }

            })

            socket.on('req:product:create', async (params: apiProduct.T.Create.Request) => {

                try{
                    console.log('req:product:create');

                    const {statusCode, data, message} = await apiProduct.Create(params, redis);

                    return io.to(socket.id).emit('res:product:create', {statusCode, data, message});

                }catch (error) { console.log(error) }

            })

            socket.on('req:product:delete', async (params: apiProduct.T.Delete.Request) => {

                try{
                    console.log('req:product:delete');

                    const {statusCode, data, message} = await apiProduct.Delete(params, redis);

                    return io.to(socket.id).emit('res:product:delete', {statusCode, data, message});

                }catch (error) { console.log(error) }

            })

            //Shop
            socket.on('req:shop:view', async (params: apiShop.T.View.Request) => {

                try{
                    console.log('req:shop:view');

                    const {statusCode, data, message} = await apiShop.View(params, redis);

                    return io.to(socket.id).emit('res:shop:view', {statusCode, data, message});

                }catch (error) { console.log(error) }

            })

            socket.on('req:shop:create', async (params: apiShop.T.Create.Request) => {

                try{
                    console.log('req:shop:create');

                    const {statusCode, data, message} = await apiShop.Create(params, redis);

                    return io.to(socket.id).emit('res:shop:create', {statusCode, data, message});

                }catch (error) { console.log(error) }

            })

            socket.on('req:shop:delete', async (params: apiShop.T.Delete.Request) => {

                try{
                    console.log('req:shop:delete');

                    const {statusCode, data, message} = await apiShop.Delete(params, redis);

                    return io.to(socket.id).emit('res:shop:delete', {statusCode, data, message});

                }catch (error) { console.log(error) }

            })

        });
    }
);
