import * as user from "users";
import * as product from "products";
import * as shop from "shop";

async function f() {

    try {

        await user.SyncDB({force:true});
        await product.SyncDB({force:true});
        await shop.SyncDB({force:true});

        user.run();
        product.run();
        shop.run();

    } catch (error) { console.error(error); }

}

f().finally();
