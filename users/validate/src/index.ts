import Joi from 'joi';
import * as T from './types';

export async function create(params: T.Create.Request): Promise<T.Create.Response> {

    try{

        const schema = Joi.object({
            username: Joi.string().required(),
            fullName: Joi.string().required(),
            phone: Joi.number().required(),
            image: Joi.string().uri().required()
        });

        const result = await schema.validateAsync(params);

        if( params.username === ('admin' || 'root' || 'su')){
            throw { statusCode: 'error', message: 'Nombre reservado para uso interno' };
        }

        return {statusCode: 'success', data: params};

    }catch (error) {

        console.error( { step: "Validation create", error: error.toString() } );

        throw { statusCode: 'error', message: error.toString() };
    }

}

export async function del(params: T.Delete.Request): Promise<T.Delete.Response> {

    try{

        const schema = Joi.object({
            username: Joi.string().required(),
        });

        const result = await schema.validateAsync(params);

        return {statusCode: 'success', data: params};

    }catch (error) {

        console.error( { step: "Validation create", error: error.toString() } );

        throw { statusCode: 'error', message: error.toString() };
    }

}

export async function update(params: T.Update.Request): Promise<T.Update.Response> {

    try{

        const schema = Joi.object({
            username: Joi.string(),
            fullName: Joi.string(),
            phone: Joi.number(),
            image: Joi.string()
        });

        const result = await schema.validateAsync(params);

        return {statusCode: 'success', data: params};

    }catch (error) {

        console.error( { step: "Validation update", error: error.toString() } );

        throw { statusCode: 'error', message: error.toString() };
    }

}

export async function view(params: T.View.Request): Promise<T.View.Response> {

    try{

        const schema = Joi.object({
            offset: Joi.number(),
            limit: Joi.number(),
            state: Joi.boolean()
        });

        const result = await schema.validateAsync(params);

        return {statusCode: 'success', data: params};

    }catch (error) {

        console.error( { step: "Validation view", error: error.toString() } );

        throw { statusCode: 'error', message: error.toString() };
    }

}

export async function findOne(params: T.FindOne.Request): Promise<T.FindOne.Response> {

    try {

        const schema = Joi.object({
            username: Joi.string(),
            id: Joi.number()
        }).xor('username', 'id');

        const result = await schema.validateAsync(params);

        return {statusCode: 'success', data: params};

    }catch (error) {

        console.error( { step: "Validation findOne", error: error.toString() } );

        throw { statusCode: 'error', message: error.toString() };
    }

}