import * as T  from '../types';
import * as Models from "../models";
import {InternalError} from "../settings";
import * as Validation from "validate-users";
// import * as Validation from "../../../validate/dist";

export async function create(params: T.Service.Create.Request): Promise<T.Service.Create.Response> {

    try{

        await Validation.create(params);

        const findOne = await Models.findOne({where:{ username: params.username }});

        if ( findOne.statusCode !== 'notFound' ){

            switch (findOne.statusCode) {

                case 'success' : return { statusCode: 'error', message: 'Usuario ya registrado'};

                default: return { statusCode: 'error', message: InternalError};

            }
        }

        const {statusCode, data, message} = await Models.create(params);

        return {statusCode, data, message};

    }catch (error) {

        console.error( { step: "Service create", error: error.toString() } );

        return { statusCode: 'error', message: InternalError};
    }

}

export async function del(params: T.Service.Delete.Request): Promise<T.Service.Delete.Response> {

    try{

        await Validation.del(params);

        let where: T.Models.Where = {username: params.username }

        const findOne = await Models.findOne({ where });

        if ( findOne.statusCode !== 'success' ){

            switch (findOne.statusCode) {

                case 'notFound' : return { statusCode: 'error', message: 'Usuario no existe.'};

                default: return { statusCode: 'error', message: InternalError};

            }
        }

        const {statusCode, message} = await Models.del({ where });

        if (statusCode !== 'success') return {statusCode, message};

        return { statusCode: 'success', data: findOne.data };

    }catch (error) {

        console.error( { step: "Service create", error: error.toString() } );

        return { statusCode: 'error', message: InternalError};
    }

}

export async function update(params: T.Service.Update.Request): Promise<T.Service.Update.Response> {

    try{

        await Validation.update(params);

        let where: T.Models.Where = {username: params.username, state: true };

        const findOne = await Models.findOne({ where });

        if ( findOne.statusCode !== 'success' ){

            switch ( findOne.statusCode ) {

                case 'notFound' : return { statusCode: 'error', message: 'Usuario no existe.'};

                default: return { statusCode: 'error', message: InternalError};

            }
        }

        if(!findOne.data.state) return {statusCode: 'notPermitted', message: 'Tu usuario no esta habilitado.'};

        const {statusCode, data, message} = await Models.update( params, { where });

        if (statusCode !== 'success') return { statusCode, message };

        return { statusCode: 'success', data: data[1][0]};

    }catch (error) {

        console.error( { step: "Service update", error: error.toString() } );

        return { statusCode: 'error', message: InternalError};
    }

}

export async function view(params: T.Service.View.Request): Promise<T.Service.View.Response> {

    try{

        await Validation.view(params);

        let where: T.Models.Where = { }

        const optionals: T.Models.Attributes[] = ['state'];

        for (let x of optionals) if (params[x] !== undefined) optionals[x] = params[x];



        const {statusCode, data, message} = await Models.findAndCountAll(   {where} );

        if (statusCode !== 'success') return { statusCode, message };

        return { statusCode: 'success', data: data};

    }catch (error) {

        console.error( { step: "Service view", error: error.toString() } );

        return { statusCode: 'error', message: InternalError};
    }

}

export async function findOne(params: T.Service.FindOne.Request): Promise<T.Service.FindOne.Response> {

    try {

        await Validation.findOne(params);

        let where: T.Models.Where = { };

        const optionals: T.Models.Attributes[] = ['username', 'id'];

        for (let x of optionals) if (params[x] !== undefined) optionals[x] = params[x];

        const { statusCode, data, message } = await Models.findOne(   {where} );

        return { statusCode, data, message};

    }catch (error) {

        console.error( { step: "Service findOne", error: error.toString() } );

        return { statusCode: 'error', message: InternalError};
    }

}