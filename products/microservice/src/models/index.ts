import * as S from 'sequelize';
import { sequelize, name } from "../settings";
import { Models as T} from "../types";

export const Model = sequelize.define<T.Model>(name,{

    name: { type: S.DataTypes.STRING },
    mark: { type: S.DataTypes.STRING },
    year: { type: S.DataTypes.STRING },
    bodega: { type: S.DataTypes.STRING },
    code: { type: S.DataTypes.STRING },
    image: { type: S.DataTypes.STRING },
    state: { type: S.DataTypes.BOOLEAN, defaultValue: true },
    lote: { type: S.DataTypes.BIGINT }

}, {freezeTableName: true})

export async function SyncDB(options?: T.SyncDB.Opts): Promise<T.SyncDB.Response>{

    try {

        const model: T.Model = await Model.sync(options)

        return {statusCode: 'success', data: model}

    }catch (error){

        console.error({ step: 'Models SyncDB', error: error.toString() })

        return {statusCode: 'error', message: error.toString()};
    }

}

export async function count(options?: T.Count.Request): Promise<T.Count.Response> {

    try {

        const instance: number = await Model.count(options);

        return {statusCode: 'success', data: instance};

    }catch (error){

        console.error({ step: 'Models count', error: error.toString() });

        return {statusCode: 'error', message: error.toString()};
    }
}

export async function create(values: T.Create.Request, options?: T.Create.Opts): Promise<T.Create.Response>{

    try {

        const instance = await  Model.create(values, options);

        return {statusCode: 'success', data: instance.toJSON()};

    }catch (error){

        console.error({ step: 'Models create', error: error.toString() })

        return {statusCode: 'error', message: error.toString()};
    }
}

export async function del(options?: T.Delete.Opts): Promise<T.Delete.Response>{

    try {

        const instance: number = await Model.destroy(options);

        return {statusCode: 'success', data: instance};

    }catch (error){

        console.error({ step: 'Models del', error: error.toString() })

        return {statusCode: 'error', message: error.toString()};
    }
}

export async function findAndCountAll(options?:  T.FindAndCountAll.Opts): Promise<T.FindAndCountAll.Response>{

    try {

        var options: T.FindAndCountAll.Opts = { ...{ limit: 12, offset: 0 }, ...options };

        const { count, rows }: {rows: T.Model[], count: number} = await Model.findAndCountAll(options);

        return {
            statusCode: 'success',
            data: { data: rows.map( (v) => v.toJSON()), itemCount: count, pageCount: Math.ceil(count/options.limit) }
        };

    }catch (error){

        console.error({ step: 'Models findAndCountAll', error: error.toString() })

        return {statusCode: 'error', message: error.toString()};
    }
}

export async function findOne(options?: T.FindOne.Opts): Promise<T.FindOne.Response>{

    try {

        const instance: T.Model | null = await Model.findOne(options);

        if(instance) return {statusCode: 'success', data: instance.toJSON() };

        else return {statusCode: 'notFound', message: 'notFound'};

    }catch (error){

        console.error({ step: 'Models findOne', error: error.toString() })

        return {statusCode: 'error', message: error.toString()};
    }
}

export async function update(values: T.Update.Request, options?: T.Update.Opts): Promise<T.Update.Response>{

    try {

        var options: T.Update.Opts = { ...{returning: true}, ...options };

        const instances: [number, T.Model[]] = await Model.update(values, options);

        return {statusCode: 'success', data: [ instances[0], instances[1].map(value => value.toJSON()) ] };

    }catch (error){

        console.error({ step: 'Models update', error: error.toString() })

        return {statusCode: 'error', message: error.toString()};
    }
}

