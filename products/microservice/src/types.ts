import * as S from 'sequelize';

export type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'validationError';

export namespace Models {

    export const attributes = [
        'id', 'name', 'mark', 'year', 'image', 'bodega', 'lote', 'state', 'createdAt', 'updatedAt'
    ] as const;

    export type Attributes = typeof attributes[number];

    export type Where = S.WhereOptions<ModelAttributes>;

    export interface ModelAttributes {

        id?: number;
        name?: string;
        mark?: string;
        year?: string;
        bodega?: string;
        code?: string;
        image?: string;
        state?: boolean;
        lote?: number;
        createdAt?: string;
        updatedAt?: string;

    }

    export interface Model extends  S.Model<ModelAttributes> {
    }

    export interface Paginate {
        data: ModelAttributes[],
        itemCount: number,
        pageCount: number,
    }

    export namespace SyncDB {

        export interface Opts extends S.SyncOptions{
        }

        export interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }

    }

    export namespace Count {

        export interface Request extends Omit<S.CountOptions<ModelAttributes>, "group"> {
        }

        export interface Response {
            statusCode: statusCode;
            data?: number;
            message?: string;
        }

    }

    export namespace Create{

        export interface Request extends ModelAttributes{
        }

        export interface Opts extends S.CreateOptions<ModelAttributes>{
        }

        export interface Response{
            statusCode: statusCode;
            data?: ModelAttributes;
            message?: string;
        }

    }

    export namespace Delete{

        export interface Opts extends S.DestroyOptions<ModelAttributes>{
        }

        export interface Response{
            statusCode: statusCode;
            data?: number;
            message?: string;
        }

    }

    export namespace FindAndCountAll{

        export interface Opts extends Omit<S.FindAndCountOptions<ModelAttributes>, "group">{
        }

        export interface Response{
            statusCode: statusCode;
            data?: Paginate;
            message?: string;
        }

    }

    export namespace FindOne{

        export interface Opts extends Omit<S.FindAndCountOptions<ModelAttributes>, "group">{
        }

        export interface Response{
            statusCode: statusCode;
            data?: ModelAttributes;
            message?: string;
        }

    }

    export namespace Update{

        export interface Request extends ModelAttributes{
        }

        export interface Opts extends S.UpdateOptions<ModelAttributes>{
        }

        export interface Response{
            statusCode: statusCode;
            data?: [ number, ModelAttributes[] ];
            message?: string;
        }

    }

}

export namespace Controller{

    export namespace Publish{

        export interface Request {
            channel: string;
            instance: string;
        }

        export interface Response {
            statusCode: statusCode;
            data?: Request;
            message?: string;
        }

    }


}

export namespace Service{

    export namespace Create{

        export interface Request{

            name: string,
            mark: string,

            year?: string,
            image?: string,
            bodega?: string,
            lote?: number,

        }

        export interface Response {
            statusCode: statusCode;
            data?: Models.ModelAttributes;
            message?: string;
        }

    }

    export namespace Delete{

        export interface Request {
            ids?: number[];

            marks?: string[],

            years?: string[],

            bodegas?: string[],
            lotes?: number[],

            state?: boolean
        }

        export interface Response {
            statusCode: statusCode;
            data?: number;
            message?: string;
        }

    }

    export namespace Update{

        export interface Request {
            id: number;
            name?: string,
            mark?: string,
            year?: string,
            image?: string,
            bodega?: string,
            lote?: number,
            state?: boolean,
        }

        export interface Response {
            statusCode: statusCode;
            data?: Models.ModelAttributes;
            message?: string;
        }

    }

    export namespace View{

        export interface Request {
            offset?: number;
            limit?: number;

            year?: string;
            state?: boolean;

            marks?: string[];
            bodegas?: string[];
            lotes?: number[];
        }

        export interface Response {
            statusCode: statusCode;
            data?: Models.Paginate;
            message?: string;
        }

    }

    export namespace FindOne{

        export interface Request {
            id: number;
        }

        export interface Response {
            statusCode: statusCode;
            data?: Models.ModelAttributes;
            message?: string;
        }

    }

}

export namespace Adapters{

    export const endpoint = ['create', 'update', 'delete', 'findOne', 'view'] as const;

    export type Endpoint = typeof endpoint[number];

    export namespace BullConn {

        export interface opts {
            concurrency: number;
            redis: Settings.REDIS;
        }
    }

    export namespace Create{

        export interface Request {
            fullName: string;
            phone: string;
            image: string;
        }

        export interface Response {
            statusCode: statusCode;
            data?: string;
            message?: string;
        }

    }

    export namespace Delete{

        export interface Request {
            id: number;
        }

        export interface Response {
            statusCode: statusCode;
            data?: string;
            message?: string;
        }

    }

}

export namespace Settings{

    export interface REDIS {
        host: string;
        port: number;
        password: string;
    }

}