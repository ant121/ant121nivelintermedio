export type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'validationError';

export const endpoint = ['create', 'update', 'delete', 'findOne', 'view'] as const;

export type Endpoint = typeof endpoint[number];

export interface REDIS {
        host: string;
        port: number;
        password: string;
}

export interface Model {

    id?: number;
    name?: string,
    mark?: string,
    year?: string,
    image?: string,
    bodega?: string,
    lote?: number,
    state?: boolean,
    createdAt?: string,
    updatedAt?: string

}

export interface Paginate {
    data: Model[],
    itemCount: number,
    pageCount: number,
}


export namespace Create{

    export interface Request{

        name: string,
        mark: string,

        year?: string,
        image?: string,
        bodega?: string,
        lote?: number,

    }

    export interface Response {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }

}

export namespace Delete{

    export interface Request {
        ids?: number[];

        marks?: string[],

        years?: string[],

        bodegas?: string[],
        lotes?: number[],

        state?: boolean
    }

    export interface Response {
        statusCode: statusCode;
        data?: number;
        message?: string;
    }

}

export namespace Update{

    export interface Request {
        id: number;
        name?: string,
        mark?: string,
        year?: string,
        image?: string,
        bodega?: string,
        lote?: number,
        state?: boolean,
    }

    export interface Response {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }

}

export namespace View{

    export interface Request {
        offset?: number;
        limit?: number;

        year?: string;
        state?: boolean;

        marks?: string[];
        bodegas?: string[];
        lotes?: number[];
    }

    export interface Response {
        statusCode: statusCode;
        data?: Paginate;
        message?: string;
    }

}

export namespace FindOne{

    export interface Request {
        id: number;
    }

    export interface Response {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }

}
