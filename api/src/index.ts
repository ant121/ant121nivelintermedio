import {io} from "socket.io-client";

const socket = io("http://localhost", {
   // auth: {
   //     token: ""
   // }
});

async function main(){

    try{

        setTimeout(()=> console.log(socket.id), 1500);

        socket.on('res:user:view', ({statusCode, data, message})=>{

            console.log('res:user:view', {statusCode, data, message});

        });

        socket.on('connect_error', (err)=>{

            console.log('res:users:view', err.message);

        });

        setTimeout(()=>{

            socket.emit('req:user:view', ({}));

        }, 300);

    }catch (e) {

    }

}

main();